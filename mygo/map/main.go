package main

import "fmt"

func main() {
	// //---------syntax------
	//// mapName:=map[keytype]valuetype{}
	colors:=map[string]string{
       "red":"danger",
	   "green":"nature",
	}
	fmt.Println(colors)
     colors["yellow"]="sun"
	 colors["yellow"]="tree"
	 fmt.Println(colors)
	 delete(colors,"yellow")
     delete(colors,"sun") // doesn't gets deleted when we put value
	 fmt.Println(colors)
// //----------call function
      printMap(colors)
}
// //iterate map
func printMap(c map[string]string){
	for colors,def:= range c{
		println(colors + " is " + def)
	}
}