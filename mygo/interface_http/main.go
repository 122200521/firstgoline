package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)


func main() {
  resp,err:= http.Get("http://google.com")	
  if err != nil{
	// ----------error handling code-----------
     fmt.Println("error:" , err)
	 os.Exit(1)
  }
//  // ----------no err---------
//    fmt.Println(resp)

// //   bs:=[]byte
//  bs:=make([]byte,99999) // size(99999 empty elements)
//  resp.Body.Read(bs)
//  fmt.Println(string(bs))
// // new line .. easy way to read
 io.Copy(os.Stdout,resp.Body)
}
