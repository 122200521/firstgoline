package main

import "testing"
func TestNewDeck(t *testing.T){
	d :=newDeck()
	//**test1 checking the lenght of new deck
	if len(d) !=16{
		t.Errorf("Expected deck length of 20 but got %d",len(d))
	}
	// **test2 checking whether the index matches the value or not
	if d[0]!="Ace of Spades"{
		t.Errorf("Expected is Ace of Spades but got %v",d[0])
	}
	//**test 3
	if d[len(d)-1] != "Four of clubs"{
		t.Errorf("Expected four of clubs but got %v",d[len(d)-1])
	}
}
