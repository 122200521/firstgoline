package main

func main() {
	// var card string="Ace of Spades"
	// fmt.Println(card)
// *********Shorthand variable declaration
	// card:=newCard() **** you can pull from other function body
	// fmt.Println(card)
// *****Slice***
	//  cards:=[]string{"Ace of Diamonds",newCard()}
	// fmt.Println(cards)
// ***shorthand form to create a slice******
	// var cards []byte
	// cards=make([]byte,5,5)
	// fmt.Println(cards)
//****to add new element into a slice we use append()method
    // cards=append(cards,"Six of Spades")
	// fmt.Println(cards)
	// for i,v := range cards{
	// 	fmt.Println(i,v)
	// }
// ********when you dont want the index
	// for _,v := range cards{
	// 	fmt.Println(v)
	// }
// *******you dont want values
	// for i:= range cards{
	// 	fmt.Println(i)
	// }
// ******deck
	//  cards:=deck{"Ace of spades","one of club","Ace of heart"}
    // fmt.Println(cards)
// *****from print function .. to return the print function
    // cards.print()
//******from function newDeck()
//  cards:=newDeck()
// 	cards.print()

// **deal
dec :=newDeck()
// cardsinHand,remainingCards :=deal(dec,5)
// cardsinHand.print()
// fmt.Println("remaining cards in deck")
// remainingCards.print()
// *****FIle IO
// fmt.Println(dec.toString())
// }
// **pass name of the file created*********
dec.saveToFile("my_file")

// func newCard()string{
// 	return "Ace of Spades" 
// // ---------readfile
cards:=newDeckFromFile("my_file")
cards.print()
// ----------shuffle function-
// dec.shuffle()
// dec.print()
}