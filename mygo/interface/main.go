package main

import "fmt"

type englishBot struct{}
type dzongkhaBot struct{}
type bot interface{
	greet() string
}
func main() {
	// // ----------cannot use shorthand declaration for global variable 
 eb:=englishBot{}
 db:=dzongkhaBot{}

 printGreeting(eb)
 printGreeting(db)

}
func (englishBot) greet() string{
	return "hello"
}
func (dzongkhaBot) greet() string{
	return "kuzuzangpo"
}
func printGreeting(b bot){
	fmt.Println(b.greet())
}


