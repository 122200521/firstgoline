package main

import (
	"fmt"
	"os"
	"strings"
)

type deck []string
// ********reciever function to print list of cards in deck 
func (d deck)print(){ //call print() in main.go
	for i,v := range d {
			fmt.Println(i,v)
		}
}
// ***to print list of cards in deck
// not a reciever function because 
func newDeck()deck{
	cards:=deck{} //create empty deck
	cardSuits:=[]string{"Spades","Hearst","clubs","Diamond"}
	cardsValue:=[]string{"Ace","Two","Three","Four","Five","King"}

	for _,s:=range cardSuits{
		for _,v:=range cardsValue{
			cards=append(cards,v+" "+"of"+" "+s)
		}
	}
	return cards
}

func deal(d deck,handsize int)(deck,deck){
    cardsinHand:=d[:handsize]
	remainingCard:=d[handsize:]
	return cardsinHand,remainingCard
}

// ***********file IO

// helper function to convert slice string to byte slice
func (d deck)toString()string{ //first  convert to single string  
	return strings.Join([]string(d),",") //to convert []string to single string, import Join() from string package 
	
}
func (d deck)saveToFile(fileName string)error{
	// writeFIle()
	 str:= newDeck().toString()
	err :=os.WriteFile(fileName, []byte(str),0666)
	return err

}
// readfile
func newDeckFromFile(filename string) deck{
	byteSlice,err := os.ReadFile(filename)
	if err!=nil{
		fmt.Println("error",err)
		os.Exit(1)
	}
	
	// convert byteSlice to deck
	str:=strings.Split(string(byteSlice),",")
	return deck(str)
}
// // shuffle
// func (d deck) shuffle(){
// 	for i:=range d{
// 		//generate random index
// 		newIndex:=rand.Intn(len(d)-1)
// 		//swap
// 		d[i],d[newIndex]=d[newIndex],d[i]
// 	}
// }
