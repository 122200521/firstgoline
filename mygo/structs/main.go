package main

import "fmt"

type contactInfo struct{
	email string
	mobile int
}
type person struct {
	firstName string
	lastName string
	// embedding new type
	address contactInfo
}
func main(){
	// person1:=person{firstName:"Pema",lastName: "Tshoki"}
	// person1:= person{"Pema","Tshoki"}
	// fmt.Println((person1))
//     var person2 person 
// 	// *** how to update value of struct
//     person2.firstName="tshering"
// 	person2.lastName="yangki"
// 	fmt.Println(person2)
// // ***"%+v" is the format
// 	fmt.Printf("%+v",person2)
//  we can sither do it as below
person2:=person{
	firstName: "Pema",
	lastName: "Tshoki",
	address: contactInfo{
		email: "ptshoki308@gmail.com",
		mobile: 77798965,
	},
}
// fmt.Println(person2)
// fmt.Printf("%+v",person2)
// //==========struct with receiver function============
// person2.print()
//==========update the first name====
// person2.updateName("Deki")  //won't work because we didnt use pointer
// fmt.Println(person2)
////----using pointer to update ------
// person2Pointer :=&person2
// person2.print()
// person2Pointer.updateName("Deki")
// person2.print()
 //***********check memory address
// fmt.Printf("%p", person2Pointer)
// // now since the reciever type in pointer you can update it in the initial variable 
person2.updateName("Deeeeche")
person2.print()
fmt.Println(person2)
}




func(p person) print(){
	fmt.Printf("%+v",p)
}
 func (pPointer  *person) updateName(newfirstName string){
	(*pPointer).firstName=newfirstName
 }

